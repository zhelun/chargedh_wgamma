#!/usr/bin/python
import sys
import os
import argparse #require python3

#run it like:
#python3 addVariables.py -Type vec_double --Name myVec


#Input:
parser=argparse.ArgumentParser()
parser.add_argument("--Type","-t",help="set variable type")
parser.add_argument("--Name","-n",help="set variable name")
args=parser.parse_args()

if args.Type:
	v_type=args.Type

	if (v_type=="vec_double")or(v_type=="vec_int")or(v_type=="vec_float"):
      	  v_type="std::vector<"+v_type[4:]+">"
	print(v_type)

if args.Name:
	name=args.Name
	cxx_name=name

main_dir=os.getcwd()
cxx_file=main_dir+"/Root/TreeSkeleton.cxx"
header_file=main_dir+"/VBSWgamma/TreeSkeleton.h"

#Marker comment:
header_marker="//headerInsertVariable"
header_line=v_type+" "+cxx_name+";"

cxx_tree_marker="//cxxInsertTree"
cxx_tree_line="  thetree->Branch(\""+name+"\",&"+cxx_name+");"

cxx_var_marker="//cxxInsertVariable"
if (v_type=="double")or(v_type=="int")or(v_type=="float")or(v_type=="bool"):
	cxx_var_line=cxx_name+"=0;"
elif (v_type=="std::vector<double>")or(v_type=="std::vector<int>")or(v_type=="std::vector<float>"):
	cxx_var_line=cxx_name+"={};"


#Declare variable in header:
with open(header_file,"r") as file:
    lines=file.readlines()
    for i in range(len(lines)):
       if header_marker in lines[i]:
           lines[i]="  "+header_line+"\n"+header_marker+"\n"      
with open(header_file,"w") as file:
    file.writelines(lines)
print("Declared variable in header file:\n    ",header_line)


#modify cxx
with open(cxx_file,"r") as file:
    lines=file.readlines()
    for i in range(len(lines)):
       if cxx_tree_marker in lines[i]:
           lines[i]="  "+cxx_tree_line+"\n"+cxx_tree_marker+"\n"
           print("adding tree in cxx file:\n    ",cxx_tree_line)
       if cxx_var_marker in lines[i]:
           lines[i]="  "+cxx_var_line+"\n"+cxx_var_marker+"\n"
           print("Initializing variables in cxx file:\n    ",cxx_var_line)
with open(cxx_file,"w") as file:
    file.writelines(lines)


