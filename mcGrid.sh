cd ../../run/grid
echo -n "enter tag:"
read tag

lsetup panda


#Wy:
#runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV:mc16_13TeV.700015.Sh_228_evgamma_pty7_EnhMaxpTVpTy.deriv.DAOD_STDM4.e7947_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.700015_Wenuy.p4097_$tag
#rm -rf VB*

#runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.700016.Sh_228_mvgamma_pty7_EnhMaxpTVpTy.deriv.DAOD_STDM4.e7947_s3126_r9364_p4097  OutputDS: user.zhelun.testGrid.700016_Wmnuy.p4097_$tag
#rm -rf VB*

#runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.700017.Sh_228_tvgamma_pty7_EnhMaxpTVpTy.deriv.DAOD_STDM4.e7947_s3126_r9364_p4097  OutputDS: user.zhelun.testGrid.700017_Wtty.p4097_$tag
#rm -rf VB*

#Zy:
#runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.700011.Sh_228_eegamma_pty7_EnhMaxpTVpTy.deriv.DAOD_STDM4.e7947_s3126_r9364_p4097  OutputDS: user.zhelun.testGrid.700011_Zeey.p4097_$tag
#rm -rf VB*

#runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.700012.Sh_228_mmgamma_pty7_EnhMaxpTVpTy.deriv.DAOD_STDM4.e7947_s3126_r9364_p4097  OutputDS: user.zhelun.testGrid.700012_Zmmy.p4097_$tag
#rm -rf VB*

#runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.700013.Sh_228_ttgamma_pty7_EnhMaxpTVpTy.deriv.DAOD_STDM4.e7947_s3126_r9364_p4097  OutputDS: user.zhelun.testGrid.700013Ztty.p4097_$tag
#rm -rf VB*

#ttg:
#runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.410389.MadGraphPythia8EvtGen_A14NNPDF23_ttgamma_nonallhadronic.deriv.DAOD_STDM4.e6155_s3126_r9364_p4097  OutputDS: user.zhelun.testGrid.410389_ttg.p4097_$tag
#rm -rf VB*

#VVy
#runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.366160.Sherpa_225_N30NNLO_WZy_leptonic.deriv.DAOD_HIGG1D2.e7090_s3126_r9364_p4062  OutputDS: user.zhelun.testGrid.366160_WZy.p4062_$tag
#rm -rf VB*

#runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.366161.Sherpa_225_N30NNLO_WWy_leptonic.deriv.DAOD_HIGG1D2.e7090_e5984_s3126_r9364_r9315_p4060  OutputDS: user.zhelun.testGrid.366161_WWy.p4060_$tag
#rm -rf VB*

#runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.366162.Sherpa_225_N30NNLO_ZZy_leptonic.deriv.DAOD_HIGG1D2.e7090_e5984_s3126_r9364_r9315_p4062  OutputDS: user.zhelun.testGrid.366162_ZZy.p4062_$tag
#rm -rf VB*

#tWg
#runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.412120.MadGraphPythia8EvtGen_A14NNPDF23_tWgamma.deriv.DAOD_STDM4.e7476_a875_r9364_p4097  OutputDS: user.zhelun.testGrid.412120_tWg.p4097_$tag
#rm -rf VB*

#runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.412006.MadGraphPythia8EvtGen_A14NNPDF23_tWgamma_GamFromDec.deriv.DAOD_STDM4.e7698_a875_r9364_p4097 OutputDS: user.zhelun.testGrid.412006_tWg.p4097_$tag
#rm -rf VB*

#WenuJets:
#runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.364170.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_STDM4.e5340_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.364170_WenuJets.p4097_$tag
rm -rf VB*

#runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.364171.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_STDM4.e5340_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.364171_WenuJets.p4097_$tag
rm -rf VB*

#runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.364172.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_BFilter.deriv.DAOD_STDM4.e5340_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.364172_WenuJets.p4097_$tag
rm -rf VB*

#runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.364173.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_STDM4.e5340_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.364173_WenuJets.p4097_$tag
rm -rf VB*

#runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.364174.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_STDM4.e5340_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.364174_WenuJets.p4097_$tag
rm -rf VB*

#runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.364175.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV70_140_BFilter.deriv.DAOD_STDM4.e5340_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.364175_WenuJets.p4097_$tag
rm -rf VB*

#runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.364176.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_STDM4.e5340_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.364176_WenuJets.p4097_$tag
rm -rf VB*

#runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.364177.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV140_280_CFilterBVeto.deriv.DAOD_STDM4.e5340_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.364177_WenuJets.p4097_$tag
rm -rf VB*

#runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.364178.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV140_280_BFilter.deriv.DAOD_STDM4.e5340_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.364178_WenuJets.p4097_$tag
rm -rf VB*

#runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.364179.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_STDM4.e5340_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.364179_WenuJets.p4097_$tag
rm -rf VB*

#runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.364180.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_STDM4.e5340_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.364180_WenuJets.p4097_$tag
rm -rf VB*

#runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.364181.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV280_500_BFilter.deriv.DAOD_STDM4.e5340_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.364181_WenuJets.p4097_$tag
rm -rf VB*

#runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.364182.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV500_1000.deriv.DAOD_STDM4.e5340_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.364182_WenuJets.p4097_$tag
rm -rf VB*

#runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.364183.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV1000_E_CMS.deriv.DAOD_STDM4.e5340_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.364183_WenuJets.p4097_$tag
rm -rf VB*


#Wmunu:
#runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.364156.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_STDM4.e5340_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.364156_WmnuJets.p4097_$tag
rm -rf VB*

#runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.364157.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_STDM4.e5340_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.364157_WmnuJets.p4097_$tag
rm -rf VB*


#runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.364158.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV0_70_BFilter.deriv.DAOD_STDM4.e5340_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.364158_WmnuJets.p4097_$tag
rm -rf VB*

#runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.364159.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_STDM4.e5340_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.364159_WmnuJets.p4097_$tag
rm -rf VB*

runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.364160.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_STDM4.e5340_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.364160_WmnuJets.p4097_$tag
rm -rf VB*

runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.364161.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV70_140_BFilter.deriv.DAOD_STDM4.e5340_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.364161_WmnuJets.p4097_$tag
rm -rf VB*


#Wtnu
runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.364184.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_STDM4.e5340_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.364184_WtnuJets.p4097_$tag
rm -rf VB*

runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.364185.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_STDM4.e5340_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.364185_WtnuJets.p4097_$tag
rm -rf VB*

runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.364186.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV0_70_BFilter.deriv.DAOD_STDM4.e5340_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.364186_WtnuJets.p4097_$tag
rm -rf VB*

runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.364187.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_STDM4.e5340_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.364187_WtnuJets.p4097_$tag
rm -rf VB*

runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.364188.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_STDM4.e5340_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.364188_WtnuJets.p4097_$tag
rm -rf VB*

runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.364189.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV70_140_BFilter.deriv.DAOD_STDM4.e5340_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.364189_WtnuJets.p4097_$tag
rm -rf VB*

runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.364190.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_STDM4.e5340_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.364190_WtnuJets.p4097_$tag
rm -rf VB*

runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.364191.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV140_280_CFilterBVeto.deriv.DAOD_STDM4.e5340_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.364191_WtnuJets.p4097_$tag
rm -rf VB*

runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.364192.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV140_280_BFilter.deriv.DAOD_STDM4.e5340_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.364192_WtnuJets.p4097_$tag
rm -rf VB*

runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.364193.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_STDM4.e5340_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.364193_WtnuJets.p4097_$tag
rm -rf VB*

runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.364194.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_STDM4.e5340_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.364194_WtnuJets.p4097_$tag
rm -rf VB*

runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.364195.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV280_500_BFilter.deriv.DAOD_STDM4.e5340_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.364195_WtnuJets.p4097_$tag
rm -rf VB*

runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.364196.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV500_1000.deriv.DAOD_STDM4.e5340_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.364196_WtnuJets.p4097_$tag
rm -rf VB*

runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.364197.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV1000_E_CMS.deriv.DAOD_STDM4.e5340_s3126_r9364_p4097 OutputDS: user.zhelun.testGrid.364197_WtnuJets.p4097_$tag
rm -rf VB*


