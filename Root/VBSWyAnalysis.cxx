#include <AsgTools/MessageCheck.h>
#include <VBSWgamma/VBSWyAnalysis.h>
#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"
#include "xAODMetaData/FileMetaData.h"


// in order to distribute the algo to workers
// avoid using it with prive namespace
ClassImp(VBSWyAnalysis)

VBSWyAnalysis :: VBSWyAnalysis (const char *name )
    : HgammaAnalysis (name)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.

  ATH_MSG_INFO("VBSWyAnalysis algorithm is constructed");
}


// HGamAnalysisFramework will call its initialize
// derived classes are required to do in createOutput()
//EL::StatusCode VBSWyAnalysis :: initialize ()
//{
//  // Here you do everything that needs to be done at the very
//  // beginning on each worker node, e.g. create histograms and output
//  // trees.  This method gets called before any input files are
//  // connected.
//
//  return EL::StatusCode::SUCCESS;
//}

EL::StatusCode VBSWyAnalysis :: fileExecute ()
{
  const xAOD::CutBookkeeperContainer *completeCBC = nullptr;
  const xAOD::CutBookkeeper *allEvents = nullptr;
  //wk()->xaodEvent()->retrieveMetaInput(completeCBC, "CutBookkeepers");
  ANA_CHECK(wk()->xaodEvent()->retrieveMetaInput(completeCBC, "CutBookkeepers"));
  int maxCycle = -1;

  for (auto cbk : *completeCBC){
    if (cbk->name() == "AllExecutedEvents" && cbk->inputStream() == "StreamAOD" && cbk->cycle() > maxCycle){
      allEvents = cbk;
      maxCycle = cbk->cycle();
    }
  }
  
  if (allEvents == nullptr){
    return StatusCode::FAILURE;
  }

  for (auto cbk : *completeCBC){
    if (cbk->name() == "AllExecutedEvents" && cbk->inputStream() == "StreamAOD" && cbk->cycle() == maxCycle){
      double sumw = cbk->sumOfEventWeights();
      sum_of_weights += sumw;
    }
  }
  return StatusCode::SUCCESS;
}

EL::StatusCode VBSWyAnalysis :: createOutput()
{
  // Here you setup the histograms needed for you analysis. This method
  // gets called after the Handlers are initialized, so that the systematic
  // registry is already filled.

  //histoStore()->createTH1F("m_yy", 60, 110, 140);

  //
  auto _file = wk()->getOutputFile("MxAOD");
  thetree = new TTree("ChargedHiggs","TTree for the chargedHiggs analysis");
  ATH_MSG_INFO("Create TTree chargedHiggs and connect to output stream MxAOD");
  thetree->SetDirectory( _file );
  initialize_tree( thetree );
  DAOD_tree = new TTree("DAOD_tree","TTree for lumi, XS and Sum_of_Weights");
  DAOD_tree->SetDirectory(_file);
  DAOD_tree->Branch("luminosity",&luminosity);
  DAOD_tree->Branch("xsection",&xsection);
  DAOD_tree->Branch("geneff",&geneff);
  DAOD_tree->Branch("kfactor",&kfactor);
  DAOD_tree->Branch("xsbrfiltereff",&xsbrfiltereff);
  DAOD_tree->Branch("sum_of_weights",&sum_of_weights);

  _file = nullptr;

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode VBSWyAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  // Important to keep this, so that internal tools / event variables
  // are filled properly.
  HgammaAnalysis::execute();
  initialize_leaves();

  // use HGam wrapper as much as possible
  // only use EventInfo when the function is not supported in the wrapper
  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));

//  const xAOD::TruthParticleContainer *truthElectrons = nullptr;
//  ANA_CHECK (evtStore()->retrieve (truthElectrons, "STDMTruthElectrons"));

//  const xAOD::TruthParticleContainer *truthMuons = nullptr;
//  ANA_CHECK (evtStore()->retrieve (truthMuons, "STDMTruthMuons"));

//  const xAOD::TruthParticleContainer *truthPhotons = nullptr;
//  ANA_CHECK (evtStore()->retrieve (truthPhotons, "STDMTruthPhotons"));

  if( HG::isMC() ){
    weight_mc = eventHandler()->mcWeight(); // mcweight[0], otherwise set m_mcWeightIndex NominalWeightIndex.DSID
	weight_prw = eventHandler()->pileupWeight();
    weight_vertex = eventHandler()->vertexWeight();
  } // otherwise weight_mc = 1
  weight_all = weight_all * weight_mc * weight_prw;


  //////////////////////
  // object selection //
  //////////////////////

  // photon objects
  // TODO add collect_photon_all,cleaning,crack,id,iso etc.
  // record photon_n for object selections each event TODO
  xAOD::PhotonContainer photons_all = photonHandler()->getCorrectedContainer(); // calibration all photons
  xAOD::PhotonContainer photons_sel = photonHandler()->applySelection(photons_all); // config now apply all

  // electron objects
  // TODO add collect_photon_all,cleaning,crack,id,iso etc.
  // record photon_n for object selections each event TODO
  xAOD::ElectronContainer electrons_all = electronHandler()->getCorrectedContainer(); // calibration
  xAOD::ElectronContainer electrons_sel = electronHandler()->applySelection(electrons_all); // config now apply all

  // muon objects
  // TODO add collect_photon_all,cleaning,crack,id,iso etc.
  // record photon_n for object selections each event TODO
  xAOD::MuonContainer muons_all = muonHandler()->getCorrectedContainer(); // calibration
  xAOD::MuonContainer muons_sel = muonHandler()->applySelection(muons_all); // config now apply all cuts


  // jet objects
  //xAOD::JetContainer jets_all = jetHandler()->getCorrectedContainer(); // calibration
  //xAOD::JetContainer jets_sel = jetHandler()->applySelection( jets_all ); // config now all cuts apply
  xAOD::JetContainer jets_all = jetHandlerPFlow()->getCorrectedContainer(); // calibration
  xAOD::JetContainer jets_sel = jetHandlerPFlow()->applySelection( jets_all ); // config now all cuts apply
  nSelectedJets=jets_sel.size();


  // overlap removal
  // TODO now use HGam one TODO
  overlapHandler()->removeOverlap(photons_sel, jets_sel, electrons_sel, muons_sel);


  std::sort( photons_sel.begin(), photons_sel.end(), order_pT );
  std::sort( electrons_sel.begin(), electrons_sel.end(), order_pT );
  std::sort( muons_sel.begin(), muons_sel.end(), order_pT );
  std::sort( jets_sel.begin(), jets_sel.end(), order_pT );

  ///////////////////
  //Event Selection//
  ///////////////////
  
  // Check if it passes OR: Configure cut in EventHandler.cxx
  dsid = eventHandler()->mcChannelNumber();
  //only check ttbar and Vy:
  if((dsid == 410470)|| (dsid==410471)|| (dsid==410472)||(dsid==700015)||(dsid==700016)||(dsid==700017)) passOR=eventHandler()->passVGammaOR();

  //nocut
  bool pass_init = 1;
  bool pass_all = pass_init;
  if( pass_init ) cutflow_fill("init", 1, weight_all);

  //triggers
  pass_trigger = eventHandler()->passTriggers();
  pass_all &= pass_trigger;
  if( pass_all ) cutflow_fill("trigger", 1, weight_all);

  //dataquality
  pass_dq = eventHandler()->passDQ();
  pass_all &= pass_dq;
  if( pass_all ) cutflow_fill("data quatlity", 1, weight_all);

  //jetEventCleaning
  pass_jetclean=false;
  static SG::AuxElement::ConstAccessor<char> eventClean_LooseBadJet("DFCommonJets_eventClean_LooseBad");
  if( HG::isData() ){
    if( eventClean_LooseBadJet(*eventInfo) )  pass_jetclean = 1;
  }else{ // MC is always clean
    pass_jetclean = 1;
  }
  pass_all &= pass_jetclean;
  if( pass_all ) cutflow_fill("jet clean", 1, weight_all);

//select leptons
  xAOD::ElectronContainer electrons=ElectronSelection(electrons_sel);
  xAOD::MuonContainer muons=MuonSelection(muons_sel);
  std::vector<xAOD::IParticle*> leptons=LeptonSelection(electrons,muons);
  const xAOD::IParticle* leaLep=0;
  nLep=leptons.size();
  if(nLep>0) leaLep=leptons.at(0);
  Recording3(leptons,leaLep_pt,leaLep_phi,leaLep_eta,lepton2_pt,lepton2_phi,lepton2_eta,lepton3_pt,lepton3_phi,lepton3_eta);
//lepton selection
  bool pass_lepton=false;
  if (nLep>0) pass_lepton=true;
  pass_all &= pass_lepton;
  if( pass_all ) cutflow_fill("one lepton", 1, weight_all);

//select photons, with the first one in the vector being the closestPhoton to the given Lepton.
  std::vector<xAOD::Photon*> photons=ClosestPhoton(photons_sel,leaLep_phi,leaLep_eta);
  xAOD::Photon* photon_close=0;
  nPhoton=photons.size();
  if(nPhoton>0) photon_close=photons.at(0);
  Recording3(photons,photon_pt,photon_phi,photon_eta,photon2_pt,photon2_phi,photon2_eta,photon3_pt,photon3_phi,photon3_eta);
  lepPho_dR=DeltaR(leaLep_phi,leaLep_eta,photon_phi,photon_eta);
//photon selection
  bool pass_photon=false;
  if(nPhoton>0) pass_photon=true;
  pass_all &= pass_photon;
  if( pass_all ) cutflow_fill("one photon", 1, weight_all);

  //Jets selection cuts at pT=20
  nSelectedJets=jets_sel.size();
  bool pass_jets=false;
  if(nSelectedJets<=2) pass_jets=true;
  xAOD::Jet* leaJet=0;
  xAOD::Jet* subLeaJet=0;
  //Record leading Jet kinematics

  if(nSelectedJets>0){
	for (auto jet : jets_sel){
		double jetPt=jet->pt()*HG::invGeV;
		if(jetPt>leaJet_pt) {
			subLeaJet_pt=leaJet_pt;
			subLeaJet=leaJet;
			leaJet_pt=jetPt;
			leaJet=jet;
		}else if(jetPt>subLeaJet_pt){
			subLeaJet_pt=jetPt;
			subLeaJet=jet;			
		}		
	}

	
	leaJet_phi=leaJet->phi();
	leaJet_eta=leaJet->eta();
  	if(nSelectedJets>1){
		subLeaJet_phi=subLeaJet->phi();
                subLeaJet_eta=subLeaJet->eta();
	}
  }//njet>0

  pass_all &= pass_jets;
  if( pass_all ) cutflow_fill("njets<=2", 1, weight_all);


  //B VETO
  bool pass_bVeto=false;
  xAOD::JetContainer jets_B = jetHandlerPFlow()->applyBJetSelection(jets_sel);
  if(jets_B.size()==0) pass_bVeto=true;
  pass_all &= pass_bVeto;
  if( pass_all ) cutflow_fill("B Veto", 1, weight_all);

  passKinematics=pass_lepton && pass_photon && pass_jets && pass_bVeto;
 


  //Selection1
  sel1=pass_all;


  if(passKinematics){

	//First add photon information
	SG::AuxElement::Accessor<int> truthType("truthType");
        SG::AuxElement::Accessor<int> truthOrigin("truthOrigin");
	SG::AuxElement::Accessor<float> topoetcone20_SC("topoetcone20_SC");
        SG::AuxElement::Accessor<float> topoetcone40_SC("topoetcone40_SC");

	pho_topoEtCone20=(topoetcone20_SC(*photon_close)) *.001;
        pho_topoEtCone40=(topoetcone40_SC(*photon_close)) *.001;
	pho_truType=truthType(*photon_close);
	pho_truOrigin=truthOrigin(*photon_close);

	phoPID_Loose=photonHandler()->passPIDCut(photon_close, egammaPID::PhotonIDLoose);	  
        phoPID_Tight=photonHandler()->passPIDCut(photon_close, egammaPID::PhotonIDTight);
	photonIsoE=pho_topoEtCone40 - 0.022*photon_pt;
	
 	phoIso_FixedCutTightCaloOnly=photonHandler()->passIsoCut(photon_close,HG::Iso::FixedCutTightCaloOnly);
        phoIso_FixedCutTight=photonHandler()->passIsoCut(photon_close,HG::Iso::FixedCutTight);
	phoIso_Tight=photonHandler()->passIsoCut(photon_close,HG::Iso::Tight);
	phoIso_FixedCutLoose=photonHandler()->passIsoCut(photon_close,HG::Iso::FixedCutLoose);
	phoIso_Loose=photonHandler()->passIsoCut(photon_close,HG::Iso::Loose);

	//Determine lepton type
	if(leaLep->type()==xAOD::Type::Electron){
		 electronEvent=true;
	}else{
		electronEvent=false;
	}

        //Adding electron information
 	if(electronEvent){
		auto leaElec=electrons.at(0);
		lepPID_Loose=electronHandler()->passPIDCut(leaElec,"Loose");
		lepPID_Medium=electronHandler()->passPIDCut(leaElec,"Medium");
		lepPID_Tight=electronHandler()->passPIDCut(leaElec,"Tight");
		//lepIso_FCLooseFixRad=electronHandler()->passIsoCut(leaElec,HG::Iso::FCLoose_FixedRad);
		lepIso_FCLoose=electronHandler()->passIsoCut(leaElec,HG::Iso::FCLoose);
		//lepIso_FCTightFixRad=electronHandler()->passIsoCut(leaElec,HG::Iso::FCTight_FixedRad);
		lepIso_FCTight=electronHandler()->passIsoCut(leaElec,HG::Iso::FCTight);
		elec_truthType=truthType(*leaElec);
		elec_truthOrigin=truthOrigin(*leaElec);
	}else{
		//must be photon event:
		auto leaMu=muons.at(0);
                lepPID_Loose=muonHandler()->passPIDCut(leaMu,"Loose");
                lepPID_Medium=muonHandler()->passPIDCut(leaMu,"Medium");
                lepPID_Tight=muonHandler()->passPIDCut(leaMu,"Tight");
		//lepIso_FCLooseFixRad=muonHandler()->passIsoCut(leaMu,HG::Iso::FCLoose_FixedRad);
                lepIso_FCLoose=muonHandler()->passIsoCut(leaMu,HG::Iso::FCLoose);
                //lepIso_FCTightFixRad=muonHandler()->passIsoCut(leaMu,HG::Iso::FCTight_FixedRad);
                lepIso_FCTight=muonHandler()->passIsoCut(leaMu,HG::Iso::FCTight);
		muon_truthType=truthType(*leaMu);
                muon_truthOrigin=truthOrigin(*leaMu);
	}


	
	//MET:
	xAOD::MissingETContainer *hardMet = new xAOD::MissingETContainer();
    	xAOD::AuxContainerBase *hardMetAux = new xAOD::AuxContainerBase();
    	hardMet->setStore(hardMetAux);

	etmissHandler()->doHardVertex(&photons_sel, &electrons_sel, &muons_sel, &jets_all, hardMet);

	for (auto metTerm : *hardMet) {
		if(metTerm->name()=="TST"){
			met_x = metTerm->mpx() * HG::invGeV;
			met_y = metTerm->mpy() * HG::invGeV;
			met = metTerm->met() * HG::invGeV;
		}
	    }
	delete hardMet;
        delete hardMetAux;

//ht: adding pT of photon/lepton/jets scalarly:
	for (auto pho:photons_sel) ht+=pho->pt()*HG::invGeV;
	for (auto e:electrons_sel) ht+=e->pt()*HG::invGeV;
	for (auto mu:muons_sel) ht+=mu->pt()*HG::invGeV;
	for (auto jet:jets_sel) ht+=jet->pt()*HG::invGeV;

//vectorial sum of pT (l y MET):
	double pt_lyMETx=leaLep_pt*cos(leaLep_phi)+photon_pt*cos(photon_phi)+met_x;
	double pt_lyMETy=leaLep_pt*sin(leaLep_phi)+photon_pt*sin(photon_phi)+met_y;
	pt_lyMET=mag(pt_lyMETx,pt_lyMETy);

//pq
	xAOD::IParticle::FourMom_t mtm_pho=photon_close->p4();
	xAOD::IParticle::FourMom_t mtm_lepton=leaLep->p4();
	pq=mtm_pho.Dot(mtm_lepton)*HG::invGeV*HG::invGeV;

	bool passMET=false;
        if((met>72)&&(met<220)) passMET=true;
        pass_all &= passMET;
        if( pass_all ) cutflow_fill("72<MET<220", 1, weight_all);

        bool passHt=false;
        if((ht>260)&&(ht<620)) passHt=true;
        pass_all &= passHt;
        if( pass_all ) cutflow_fill("260<Ht<620", 1, weight_all);

        bool pass_ptlyMET=false;
        if((pt_lyMET>100)&&(pt_lyMET<420)) pass_ptlyMET=true;
        pass_all &= pass_ptlyMET;
        if( pass_all ) cutflow_fill("72<lyMET<220", 1, weight_all);

        bool passPq=false;
        if((pq>3300)&&(pq<8200)) passPq=true;
        pass_all &= passPq;
        if( pass_all ) cutflow_fill("pq", 1, weight_all);


}



if (HG::isMC()) {
    static SG::AuxElement::Accessor<float> scaleFactor("scaleFactor");
    if (photons_sel.size()!=0) {
      for (auto _photon: photons_sel)
      { weight_gam *= scaleFactor(*_photon);}
    }
    if (muons_sel.size()!=0){
      for (auto _muon: muons_sel)
      { weight_m *= scaleFactor(*_muon);}
    }
    if (electrons_sel.size()!=0){
      for (auto _electron : electrons_sel)
      { weight_e *= scaleFactor(*_electron);}
    }
  weight_all=weight_all*weight_gam;
  }

  /////////////////////
  //  tree filling   //
  /////////////////////

// if data, clear weight to 1
   if( !HG::isMC() ) weight_all = 1;
// Only fill the tree for interesting events(Passing Kinematic Cuts)

   if(passKinematics) thetree->Fill ();



  return EL::StatusCode::SUCCESS;
}

//EL::StatusCode VBSWyAnalysis :: postExecute ()
//{
//  HgammaAnalysis::postExecute(); // keep this line!
//  return EL::StatusCode::SUCCESS;
//}
//
//
//
//EL::StatusCode VBSWyAnalysis::histFinalize ()
//{
//  HgammaAnalysis::histFinalize(); // keep this line!
//  return EL::StatusCode::SUCCESS;
//}

EL::StatusCode VBSWyAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.

  // Fill lumi and XS and sum of weights info
  if( HG::isMC() ){
    luminosity = eventHandler()->integratedLumi(); 
    xsection = HgammaAnalysis::getCrossSection(-1) *1000;
    geneff = HgammaAnalysis::getGeneratorEfficiency(-1);
    kfactor = HgammaAnalysis::getKFactor(-1);
    xsbrfiltereff = HgammaAnalysis::crossSectionBRfilterEff(-1);
  }
  DAOD_tree->Fill();

  // fill cutflow to histogram
  // histoStore() only register histograms to wk() in initialize()
  // it is too late now, so DIY
  unsigned int _size = cutflow_label.size();
  auto _h_count = new TH1D("chargedH_cutflow", "chargedH_cutflow", _size, 0, _size);
  auto _h_weight = new TH1D("chargedH_cutflow_weight", "chargedH_cutflow_weight", _size, 0, _size);

  for( unsigned int _i = 1; _i <= _size; ++_i ){
    _h_count->SetBinContent( _i, cutflow_count[_i-1] );
    _h_count->GetXaxis()->SetBinLabel( _i, cutflow_label[_i-1].c_str() );

    _h_weight->SetBinContent( _i, cutflow_weight[_i-1] );
    _h_weight->GetXaxis()->SetBinLabel( _i, cutflow_label[_i-1].c_str() );
  }

  wk()->addOutput( _h_count );
  wk()->addOutput( _h_weight );

  HgammaAnalysis::finalize(); // keep this line!
  // the end of which m_event->finishWritingTo(file)
  // manual closing the file cause warning
  //auto _file = wk()->getOutputFile("MxAOD");
  //_file->ls();
  //_file->Close();

  // handled tree to file, so do NOT delete by hand
  //delete thetree;

  return EL::StatusCode::SUCCESS;
}


//_________________________________________________________________________________________________
void VBSWyAnalysis :: delta2_fcn(Int_t& npar, Double_t* grad, Double_t& f, Double_t* par, Int_t iflag)
{
    
  Double_t delta2 = 0;
  Double_t alpha = par[0];
  Double_t r = par[1];
  Double_t dphi = par[2];
  Double_t l_pt = par[3];
  Double_t l_m = par[4];
  Double_t n_px = par[5];
  Double_t n_py = par[6];
  r /= sqrt(l_pt * l_pt + l_m * l_m) - l_pt * cos(dphi + alpha);
  TLorentzVector *neut = new TLorentzVector(n_px, n_py, 0., 0.);
  neut->SetE(neut->P());
  
  TLorentzVector *neut_new = new TLorentzVector(r * neut->P() * cos(neut->Phi() + alpha), r * neut->P() * sin(neut->Phi() + alpha), 0., 0.);
  neut_new->SetE(neut_new->P());
  
  delta2 = pow((neut_new->Px() - neut->Px()), 2)  + pow((neut_new->Py() - neut->Py()), 2);
  r *= sqrt(l_pt * l_pt + l_m * l_m) - l_pt * cos(dphi + alpha);
  delete neut;
  delete neut_new;
  f = delta2;
}

//_________________________________________________________________________________________________
double VBSWyAnalysis :: fitAlpha(const TLorentzVector* L, const Double_t met, const Double_t metphi)
{
  //if(m_debug>0) std::cout << "entering fitAlpha()" << std::endl;

  // initialize
  double m_mWpdg = 80.4 * 1000; //MeV
  Double_t pxNu = met * cos(metphi);
  Double_t pyNu = met * sin(metphi);
  Double_t ptNu = met;

  TMinuit *fit = new TMinuit(7);
  fit->SetFCN(delta2_fcn);
  int ierr = 0;
  double arglist[1] = {-1};
  fit->mnexcm("SET PRIN",arglist,1,ierr);
  // Initialise the parameters
  std::string par_name[7] = {"alpha", "r", "dphi", "l_pt", "l_m", "n_px", "n_py"};
  Double_t par_ival[7] = {0., (m_mWpdg * m_mWpdg - L->M() * L->M()) / (2 * ptNu), metphi - L->Phi(), L->Pt(), L->M(), pxNu, pyNu};
  Double_t par_step[7] = {0.1, 0., 0., 0., 0., 0., 0.};
  Double_t par_min[7] = {-3.15, 0., -3.15, 0., 0., -10000., -10000.};
  Double_t par_max[7] = {3.15, 1., 3.15, 10000., 80., 10000., 10000.};
  for (Int_t i = 0; i < 7; i++){
    fit->DefineParameter(i,par_name[i].c_str(),par_ival[i],par_step[i],par_min[i],par_max[i]);
    if (i != 0){
      fit->FixParameter(i);
    }
  }
  fit->SetPrintLevel(-1);
  fit->Migrad();
  Double_t a, e_a;
  Int_t ret = fit->GetParameter(0, a, e_a);
  delete fit;
  if (ret > 0){
    return a;
  }
  else {
    std::cout << "Error in fit of alpha for met correction" << std::endl;
    return 0.;
  }
  
}

//_________________________________________________________________________________________________
std::vector<TLorentzVector*> VBSWyAnalysis :: candidatesFromWMass_Rotation(const TLorentzVector* L, const Double_t met, const Double_t metphi, const bool useSmallestPz)
{

  //if(m_debug>0) std::cout << "entering candidatesFromWMassRotation()" << std::endl;
  
  // initialize
  Double_t m_mWpdg = 80.4 * 1000; //MeV
  Double_t pxNu = met * cos(metphi);
  Double_t pyNu = met * sin(metphi);
  
  Double_t pzNu = -1000000;
  Double_t ptNu = met;
  Double_t eNu;
  
  std::vector<TLorentzVector*> NC;
    
  Double_t c1 = m_mWpdg * m_mWpdg - L->M() * L->M() + 2 * (L->Px() * pxNu + L->Py() * pyNu);
  Double_t b1 = 2 * L->Pz();
    
  Double_t A = 4 * pow(L->E(), 2) - b1 * b1;
  Double_t B = -2 * c1 * b1;
  Double_t C = 4 * pow(L->E(), 2) * ptNu * ptNu - c1 * c1;
  Double_t discr = B*B - 4*A*C;
  Double_t r = 1;
  
  Double_t sol1, sol2;
  if (discr > 0){
    sol1 = (-B + sqrt(discr)) / (2*A);
    sol2 = (-B - sqrt(discr)) / (2*A);
  }
  else { 
    Double_t alpha = fitAlpha(L, met, metphi);
    //cout<<"alpha "<<alpha<<" L "<<L<<endl;
    Double_t dphi = metphi - L->Phi();
    r = ( pow(m_mWpdg,2) - pow(L->M(),2) ) / (2 * ptNu * (sqrt(pow(L->Pt(),2) + pow(L->M(),2)) - L->Pt() * cos(dphi + alpha)));
    
    Double_t old_p = ptNu;
    Double_t old_phi = metphi;
    pxNu = r * old_p * cos(old_phi + alpha);
    pyNu = r * old_p * sin(old_phi + alpha);
    ptNu = sqrt (pxNu*pxNu + pyNu*pyNu);
    
    c1 = m_mWpdg * m_mWpdg - pow(L->M(),2) + 2 * (L->Px() * pxNu + L->Py() * pyNu);
    B = -2 * c1 * b1;
    C = 4 * pow(L->E(),2) * ptNu * ptNu - c1 * c1;
    discr = B*B - 4*A*C;
    
    sol1 = -B / (2*A);
    sol2 = -B / (2*A);
  }
  
  if (useSmallestPz){
    
    pzNu = (fabs(sol1) > fabs(sol2)) ? sol2 : sol1;
    
    eNu  = sqrt(pxNu*pxNu + pyNu*pyNu + pzNu*pzNu);
    TLorentzVector *nu1 = new TLorentzVector(pxNu,pyNu,pzNu,eNu);
    NC.push_back(nu1);
    
  }else{
    
    pzNu = sol1;
    eNu  = sqrt(pxNu*pxNu + pyNu*pyNu + pzNu*pzNu);
    TLorentzVector *nu1 = new TLorentzVector(pxNu,pyNu,pzNu,eNu);
    pzNu = sol2;
    eNu = sqrt(pxNu*pxNu + pyNu*pyNu + pzNu*pzNu);
    TLorentzVector *nu2 = new TLorentzVector(pxNu,pyNu,pzNu,eNu);
    NC.push_back(nu1);
    NC.push_back(nu2);
    
  }
  
  //if(m_debug>0) std::cout << "quitting NeutrinoBuilder::candidatesFromWMassRotation() : " << NC.size() << std::endl;
  return NC;
}

