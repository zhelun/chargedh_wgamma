#ifndef VBSWgamma_Kinematics_H
#define VBSWgamma_Kinematics_H

#include <TTree.h>
//#include "xAODEgamma/PhotonAuxContainer.h"
//#include "xAODEgamma/PhotonContainer.h"
#include <HGamAnalysisFramework/HgammaAnalysis.h>


class Kinematics : public TObject
{
public:
	Kinematics () {;}
	virtual ~Kinematics() {}

 	double mag(double,double);
	double DeltaPhi(double,double);
	double DeltaEta(double,double);
	double DeltaR(double,double,double,double);
	std::vector<xAOD::Photon*> ClosestPhoton(xAOD::PhotonContainer,double,double);
	xAOD::ElectronContainer ElectronSelection(xAOD::ElectronContainer);
        xAOD::MuonContainer MuonSelection(xAOD::MuonContainer);
	std::vector<xAOD::IParticle*> LeptonSelection(xAOD::ElectronContainer,xAOD::MuonContainer);
	void Recording(xAOD::IParticle*,double&,double&,double&);
	void Recording(xAOD::Photon*,double&,double&,double&);
	void Recording3(std::vector<xAOD::IParticle*>,double&,double&,double&,double&,double&,double&,double&,double&,double&);
	void Recording3(std::vector<xAOD::Photon*>,double&,double&,double&,double&,double&,double&,double&,double&,double&);

	static bool order_pT ( const xAOD::IParticle* p1, const xAOD::IParticle* p2 ){
    return (p1->pt() > p2->pt());
}




// in order to distribute the algo to workers
private:
ClassDef(Kinematics, 1);

};
#endif
