#include <dirent.h>
#include <sys/stat.h>

vector<string> get_files(vector<string> dirList)
{
  DIR *dir;
  struct dirent *file;
  vector<string> fileList;
  vector<string> full_fileList;
  for (int i = 0; i < dirList.size(); i++){
    if ((dir = opendir((dirList.at(i)).c_str())) != NULL){
      while ((file = readdir(dir)) != NULL){
        if ((std::strcmp(file->d_name,".") && std::strcmp(file->d_name,".."))){
          //fileList.push_back(file->d_name);
          fileList.push_back((dirList.at(i)+"/"+file->d_name).c_str());
        }
      }
    }
    closedir(dir);
  }   

  return(fileList);
}

vector<string> return_file_list(vector<string> dirList)
{
  struct stat sb;
  vector<string> fileList = {};
  for (int i = 0; i < dirList.size(); i++){
    vector<string> fileList_temp = {dirList.at(i)};
    while (stat((fileList_temp.at(0)).c_str(), &sb) == 0 && S_ISDIR(sb.st_mode)){
      fileList_temp = get_files(fileList_temp);
    }
    fileList.insert(fileList.end(), fileList_temp.begin(), fileList_temp.end());  
  }

  return fileList;  
}

vector<string> return_sample_list(vector<string> dirList)
{
  struct stat sb;
  vector<string> fileList{};
  for (int i = 0; i < dirList.size(); i++){
    vector<string> fileList_temp = {dirList.at(i)};
    while ((fileList_temp.at(0)).find("MxAOD.root") == std::string::npos){
      fileList_temp = get_files(fileList_temp);
    }
    fileList.insert(fileList.end(), fileList_temp.begin(), fileList_temp.end());
  }

  return fileList;
}

//Use for testing functions
void utils()
{
  vector<string> dirList = {"/home/zhelunli/Desktop/chargedHiggs/run/tt"};  
  //vector<string> fileList = return_file_list(dirList);
  vector<string> fileList = return_sample_list(dirList);
  vector<string> xxx=return_file_list({fileList.at(0)});
  for (int i = 0; i < fileList.size(); i++){
    cout<<fileList.at(i)<<endl;
  }

  for (int i = 0; i < xxx.size(); i++){
    cout<<xxx.at(i)<<endl;
  }
}

