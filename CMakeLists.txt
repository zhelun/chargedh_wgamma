# The name of the package:
atlas_subdir (VBSWgamma)

# Declare the package dependencies:
atlas_depends_on_subdirs( PUBLIC
                          HGamCore/HGamAnalysisFramework
                        )

# Find the needed external(s):
find_package( ROOT COMPONENTS Core RIO Hist Tree )

# build a CINT dictionary for the library
atlas_add_root_dictionary ( VBSWgammaLib VBSWgammaCintDict
                            ROOT_HEADERS Root/LinkDef.h
                            EXTERNAL_PACKAGES ROOT
                          )

#Atlas dependencies
atlas_depends_on_subdirs(
  PUBLIC
  Event/xAOD/xAODCutFlow )

# build a shared library
atlas_add_library( VBSWgammaLib
                   VBSWgamma/*.h Root/*.cxx ${VBSWgammaCintDict}
                   PUBLIC_HEADERS VBSWgamma
                   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                   LINK_LIBRARIES ${ROOT_LIBRARIES}
                                   HGamAnalysisFrameworkLib
				   xAODCore
				   xAODCutFlow
				   AsgTools
                 )

# Install files from the package:
atlas_install_joboptions( share/*_jobOptions.py )
atlas_install_scripts( share/*_eljob.py )

# Install files from the package:
atlas_install_data( data/* )

atlas_add_executable( runVBSWgamma util/runVBSWgamma.cxx
                      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                      LINK_LIBRARIES ${ROOT_LIBRARIES} VBSWgammaLib )
