#include <AsgTools/MessageCheck.h>
#include <VBSWgamma/CutflowTool.h>

// in order to distribute the algo to workers
// avoid using it with prive namespace
ClassImp(CutflowTool)


void CutflowTool :: cutflow_fill( std::string label, double count, double weight ){

  // first time filling an empty cutflow
  if( cutflow_label.size() == 0 ){
    cutflow_label.push_back(label);
	cutflow_count.push_back(count);
	cutflow_weight.push_back(weight);
    return;
  }

  // filling unempty cutflow
  unsigned int _i = 0;
  for( _i=0; _i < cutflow_label.size(); ++_i ){

    std::string _lbl = cutflow_label[_i];

	// existing slot for a cut
	if( _lbl == label ){
	  cutflow_count[_i] = cutflow_count[_i] + count;
	  cutflow_weight[_i] = cutflow_weight[_i] + weight;
	  return;
	}
  }

  // add a slot for a new cut
  if( _i == cutflow_label.size() ){
    cutflow_label.push_back(label);
    cutflow_count.push_back(count);
    cutflow_weight.push_back(weight);  
  }

  return;
}

