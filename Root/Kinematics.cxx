#include <VBSWgamma/Kinematics.h>
// in order to distribute the algo to workers
// // avoid using it with prive namespace
ClassImp(Kinematics)

double Kinematics :: mag(double x, double y){
	return sqrt(x*x+y*y);
}

double Kinematics :: DeltaPhi(double x, double y){
	double deltaPhi=fmod( TMath::TwoPi()+x-y , TMath::TwoPi() );	
	if (deltaPhi > M_PI) deltaPhi = TMath::TwoPi() - deltaPhi;	
	return deltaPhi;
}


double Kinematics :: DeltaEta(double x,double y){
	auto diff=x-y;
	return diff;
}

double Kinematics :: DeltaR(double phi1,double eta1,double phi2,double eta2){
	double dphi=DeltaPhi(phi1,phi2);
	double deta=DeltaEta(eta1,eta2);
	double dR=mag(dphi,deta);
	return dR;
}

std::vector<xAOD::Photon*> Kinematics :: ClosestPhoton(xAOD::PhotonContainer input_photons,double phi,double eta){
	//returns a vector of photon With the first one being the closest to a specific lepton in phi,eta.
	std::vector<xAOD::Photon*> photons;
        //Find the closest:
	double min_dR=100000.0;
	xAOD::Photon* closest_photon=0;
	size_t closestPhoton_index=0;
	for (auto pho : input_photons){
                double cur_dR=DeltaR(pho->phi(),pho->eta(),phi,eta);
                if(cur_dR<min_dR){
                        min_dR=cur_dR;
			closest_photon=pho;
			closestPhoton_index=pho->index();
                }
        }

	if(input_photons.size()>0) photons.push_back(closest_photon);
	//record other by looping in pt order:
	std::sort(input_photons.begin(),input_photons.end(), order_pT );
	for (auto pho : input_photons){
		if(pho->index()!=closestPhoton_index) photons.push_back(pho);
	}
	return photons;
}

xAOD::ElectronContainer Kinematics :: ElectronSelection(xAOD::ElectronContainer electrons){
	xAOD::ElectronContainer selected(SG::VIEW_ELEMENTS);
	for (const auto electron : electrons){
        double e_pt=electron->pt()*HG::invGeV;
        bool pT_e=e_pt>25;
        bool eta_e=(std::abs(electron->eta())<2.5);
        if((pT_e)&&(eta_e)) selected.push_back(electron);
	}
	std::sort( selected.begin(), selected.end(), order_pT );
	return selected;
}

xAOD::MuonContainer Kinematics :: MuonSelection(xAOD::MuonContainer muons){
        xAOD::MuonContainer selected(SG::VIEW_ELEMENTS);
        for (const auto muon : muons){
        	double mu_pt=muon->pt()*HG::invGeV;
        	bool pT_mu=mu_pt>25;
        	bool eta_mu=(std::abs(muon->eta())<2.5);
      		if((pT_mu)&&(eta_mu)) selected.push_back(muon);
        }
        std::sort( selected.begin(), selected.end(), order_pT );
        return selected;
}

std::vector<xAOD::IParticle*>  Kinematics :: LeptonSelection(xAOD::ElectronContainer electrons, xAOD::MuonContainer muons){
	std::vector<xAOD::IParticle*> leptons;
	for (const auto e : electrons)	leptons.push_back(e);
	for (const auto mu : muons) leptons.push_back(mu);

        std::sort( leptons.begin(),leptons.end(), order_pT );
	return leptons;	
}


void Kinematics :: Recording(xAOD::IParticle* p,double& pt,double& phi,double& eta){
	pt=p->pt()*HG::invGeV;
	phi=p->phi();
	eta=p->eta();
	return;
}

void Kinematics :: Recording(xAOD::Photon* p,double& pt,double& phi,double& eta){
        pt=p->pt()*HG::invGeV;
        phi=p->phi();
        eta=p->eta();
        return;
}


void Kinematics :: Recording3(std::vector<xAOD::IParticle*> particle,double& p1_pt,double& p1_phi,double& p1_eta,double& p2_pt, double& p2_phi,double& p2_eta,double& p3_pt, double& p3_phi, double& p3_eta){
	int nparticles=particle.size();
	if(nparticles>2){
		Recording(particle.at(0),p1_pt,p1_phi,p1_eta);
		Recording(particle.at(1),p2_pt,p2_phi,p2_eta);
		Recording(particle.at(2),p3_pt,p3_phi,p3_eta);
	}
	if(nparticles==2){
		Recording(particle.at(0),p1_pt,p1_phi,p1_eta);
                Recording(particle.at(1),p2_pt,p2_phi,p2_eta);
	}
	if(nparticles==1) Recording(particle.at(0),p1_pt,p1_phi,p1_eta);
	
	return;
}

void Kinematics :: Recording3(std::vector<xAOD::Photon*> particle,double& p1_pt,double& p1_phi,double& p1_eta,double& p2_pt, double& p2_phi,double& p2_eta,double& p3_pt, double& p3_phi, double& p3_eta){
        int nparticles=particle.size();
        if(nparticles>2){
                Recording(particle.at(0),p1_pt,p1_phi,p1_eta);
                Recording(particle.at(1),p2_pt,p2_phi,p2_eta);
                Recording(particle.at(2),p3_pt,p3_phi,p3_eta);
        }
        if(nparticles==2){
                Recording(particle.at(0),p1_pt,p1_phi,p1_eta);
                Recording(particle.at(1),p2_pt,p2_phi,p2_eta);
        }
        if(nparticles==1) Recording(particle.at(0),p1_pt,p1_phi,p1_eta);

        return;
}


