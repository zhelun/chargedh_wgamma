#include <VBSWgamma/VBSWyAnalysis.h>
#include <VBSWgamma/TreeSkeleton.h>
#include <VBSWgamma/CutflowTool.h>
#include <VBSWgamma/Kinematics.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#endif


#ifdef __CINT__
#pragma link C++ class VBSWyAnalysis+;
#pragma link C++ class TreeSkeleton+;
#pragma link C++ class CutflowTool+;
#pragma link C++ class Kinematics+;
#endif

