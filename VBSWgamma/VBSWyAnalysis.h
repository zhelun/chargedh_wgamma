#ifndef VBSWgamma_VBSWyAnalysis_H
#define VBSWgamma_VBSWyAnalysis_H

#include <HGamAnalysisFramework/HgammaAnalysis.h>
#include <VBSWgamma/TreeSkeleton.h>
#include <VBSWgamma/CutflowTool.h>
#include <VBSWgamma/Kinematics.h>

//#include <VBSWgamma/VBSWgammaTool.h>
#include <TLorentzVector.h>
#include <TMinuit.h>
#include <vector>
#include <xAODBase/IParticle.h>
#include <algorithm>

class VBSWyAnalysis : public HgammaAnalysis, public TreeSkeleton, public CutflowTool, public Kinematics
{
private:
  // Configuration, and any other types of variables go here.
  //float m_cutValue;
  //TTree *m_myTree;
  //TH1 *m_myHist;

  TTree* thetree = nullptr;
  TTree* DAOD_tree = nullptr;

public:
  // this is a standard algorithm constructor
  VBSWyAnalysis () {}
  VBSWyAnalysis (const char *name);
  virtual ~VBSWyAnalysis() {}

  // from HgammaAnalysis
  virtual EL::StatusCode execute () ;
  virtual EL::StatusCode finalize () ;
  virtual EL::StatusCode fileExecute();
  //virtual EL::StatusCode histFinalize () ;
  //virtual EL::StatusCode postExecute () ;
  virtual EL::StatusCode createOutput () ;

  //
  bool isWev = false; // determined by finding in data/MC electron or muon
  bool isWmv = false; // determined by finding in data/MC electron or muon

  double luminosity = 0;
  double xsection = 0;
  double geneff = 1;
  double kfactor = 1;
  double xsbrfiltereff = 1;
  double sum_of_weights = 0;

  // tools
  //VBSWgammaTool::descending_pt_class descending_pt;
  //VBSWgammaTool mytool;
  // quick copy from https://twiki.nevis.columbia.edu/twiki/bin/view/Main/TopPartnerAnalysis
  static void delta2_fcn(Int_t& npar, Double_t* grad, Double_t& f, Double_t* par, Int_t iflag);
  double fitAlpha(const TLorentzVector* L, const Double_t met, const Double_t metphi);
  std::vector<TLorentzVector*> candidatesFromWMass_Rotation(const TLorentzVector* L, const Double_t met, const Double_t metphi, const bool useSmallestPz);

  static bool order_pT ( const xAOD::IParticle* p1, const xAOD::IParticle* p2 ){
    return (p1->pt() > p2->pt());
  }

  // in order to distribute the algo to workers
  ClassDef(VBSWyAnalysis, 1);
};

#endif
