This work is for the search of GM-model charged Higgs through their leptonic decays. Specifically it is looking for the charged higgs decaying into W gamma final states

This work is using the HGam framwork from: https://gitlab.cern.ch/atlas-physics/sm/ew/wgamma-vbs-run2/VBSWgamma/-/tree/master

```
mkdir hwy
cd hwy
mkdir source run build
cd source
git clone --recursive ssh://git@gitlab.cern.ch:7999/vbswy/HGamCore.git
git clone ssh://git@gitlab.cern.ch:7999/zhelun/chargedh_wgamma.git
```
Fill the following content into source/CMakeLists.txt
```
# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# This is a template for a CMakeLists.txt file that can be used in a client
# project (work area) to set up building ATLAS packages against the configured
# release.
#

# Set the minimum required CMake version:
cmake_minimum_required( VERSION 3.2 FATAL_ERROR )

# If there's a directory called AtlasCMake in the project,
# and the user didn't specify AtlasCMake_DIR yet, then let's
# give it a default value.
if( IS_DIRECTORY ${CMAKE_SOURCE_DIR}/Build/AtlasCMake AND
      NOT AtlasCMake_DIR AND NOT ENV{AtlasCMake_DIR} )
   set( AtlasCMake_DIR ${CMAKE_SOURCE_DIR}/Build/AtlasCMake )
endif()

# If there's a directory called AtlasLCG in the project,
# and the user didn't specify LCG_DIR yet, then let's
# give it a default value.
if( IS_DIRECTORY ${CMAKE_SOURCE_DIR}/Build/AtlasLCG AND
      NOT LCG_DIR AND NOT ENV{LCG_DIR} )
   set( LCG_DIR ${CMAKE_SOURCE_DIR}/Build/AtlasLCG )
endif()

# Pick up a local version of the AtlasCMake code if it exists:
find_package( AtlasCMake QUIET )

# Find the project that we depend on:
find_package( AnalysisBase )

# Set up CTest:
atlas_ctest_setup()

# Set up a work directory project:
atlas_project( WorkDir 21.2.100
   USE AnalysisBase 21.2.100
    )

# Set up the runtime environment setup script(s):
lcg_generate_env( SH_FILE ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh )
install( FILES ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh
   DESTINATION . )

# Set up CPack:
atlas_cpack_setup()
```
Setup Hgam:
```
cd source/HGamCore
git checkout vbswy-v00-hgamma_hzy_slc7_v1
cd -
```
Setup environment
```
cd build/
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
asetup 21.2.100,AnalysisBase

```

```
cd build/
cmake ../source
make -j4

```
Everytime after

Setup Environment
```
cd build/
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
asetup 21.2.100,AnalysisBase
source x86_64-*/setup.sh
```


Compile:
```
cd build/
cmake ../source
make -j4
```

To run:

```
cd run
runVBSWgamma VBSWgamma/VBSWgamma.config your_single_DAOD.root NumEvents: 2000
or
runVBSWgamma VBSWgamma/VBSWgamma.config InputFileList: file_list.txt
```

run with grid:
```
lsetup panda
runVBSWgamma VBSWgamma/VBSWgamma.config GridDS: mc16_13TeV.363270.MGaMcAtNloPy8EG_EWKVgamma.deriv.DAOD_STDM4.e7591_e5984_s3126_r9364_r9315_p3975 OutputDS: user.xiaohu.test_v04.363270.p3975

```

Modifying Athena
```
# Run from source/ directory
git clone ssh://git@gitlab.cern.ch:7999/[username]/athena.git
cd athena
git checkout 21.2
```

Then Make changes to relevant packages and compile. I also made a package_filter as suggested but ended up not using it. Just the do the simple cmake and then make is fine.

