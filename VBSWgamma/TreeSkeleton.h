#ifndef VBSWgamma_TreeSkeleton_H
#define VBSWgamma_TreeSkeleton_H

#include <TTree.h>


class TreeSkeleton : public TObject
// define the structure of the tree
// connect tree leaves to local variables
// do NOT manage the tree or its memory
// IF tree* is a member variable, TObject ClassDef tries to save the tree. Crash!
{
public:
  // this is a standard algorithm constructor
  TreeSkeleton () {;}
  virtual ~TreeSkeleton() {}

  void initialize_tree( TTree* tr ); // call in EL::Algorithm::initialize()
  void initialize_leaves(); // call in the beginning of each event in EL::Algorithm::execute()

  /////////////////////////////////////////////////////////////
  // local variables that are connected to the tree branches //
  /////////////////////////////////////////////////////////////

  // event basic info
  unsigned int runnumber = 0; ///< Run number
  unsigned long long eventnumber = 0; ///< Event number
  unsigned int dsid = 0; // dsid of mc samples

  //event selection:
  bool sel1=false;

  // variables
  double met_x = 0;
  double met_y = 0;
  double met = 0;
  double leaLep_pt=0;
  double leaLep_phi=0;
  double leaLep_eta=0;
  double photon_pt=0;
  double photon_phi=0;
  double photon_eta=0;
  double ht=0;
  double pt_lyMET=0;
  double pq=0;

  int nSelectedJets;
  int nLep;
  int nPhoton;
  double photon2_pt;
  double photon3_pt;
  double photon2_phi;
  double photon3_phi;
  double photon2_eta;
  double photon3_eta;
  double lepton2_pt;
  double lepton2_phi;
  double lepton2_eta;
  double lepton3_pt;
  double lepton3_phi;
  double lepton3_eta;
  double lepPho_dR;
  double leaJet_pt;
  double leaJet_phi;
  double leaJet_eta;
  double subLeaJet_pt;
  double subLeaJet_phi;
  double subLeaJet_eta;
  bool passOR;
  int pho_truType;
  int pho_truOrigin;
  double pho_topoEtCone20;
  double pho_topoEtCone40;
  double photonIsoE;
  bool phoPID_Loose;
  bool phoPID_Tight;
  bool phoIso_FixedCutTightCaloOnly;
  bool phoIso_FixedCutTight;
  bool phoIso_Tight;
  bool phoIso_FixedCutLoose;
  bool phoIso_Loose;
  bool electronEvent;
  bool lepPID_Loose;
  bool lepPID_Medium;
  bool lepPID_Tight;
  bool lepIso_FCLoose;
  bool lepIso_FCTight;
  double weight_all;
  double weight_mc;
  double weight_prw;
  double weight_gam;
  double weight_e;
  double weight_m;
  int elec_truthType;
  int elec_truthOrigin;
  int muon_truthType;
  int muon_truthOrigin;
  bool pass_trigger;
  bool pass_dq;
  bool pass_jetclean;
  bool passKinematics;
  double weight_vertex;
//headerInsertVariable
private:

  // in order to distribute the algo to workers
  ClassDef(TreeSkeleton, 1);

};

#endif
