#ifndef VBSWgamma_CutflowTool_H
#define VBSWgamma_CutflowTool_H

#include <TH1.h>


class CutflowTool : public TObject
{
public:
  CutflowTool() {}
  virtual ~CutflowTool() {}

  // cutflow functions
  void cutflow_fill( std::string label, double count, double weight );

  // cutflow
  std::vector<std::string> cutflow_label; // meaning for each cut
  std::vector<double> cutflow_count; // evt count accumulated for each cut
  std::vector<double> cutflow_weight; // evt weight accumulated for each cut

  // in order to distribute the algo to workers
  ClassDef(CutflowTool, 1);
};

#endif
