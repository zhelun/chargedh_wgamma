#include <AsgTools/MessageCheck.h>
#include <VBSWgamma/TreeSkeleton.h>

// in order to distribute the algo to workers
// avoid using it with prive namespace
ClassImp(TreeSkeleton)


void TreeSkeleton :: initialize_tree( TTree* tr ){

  // connect to the external tree
  TTree* thetree = tr;

  // connect local variables to branches

  // event basic info
  thetree->Branch("runnumber", &runnumber);
  thetree->Branch("eventnumber", &eventnumber);
 
  // variables  
  thetree->Branch("sel1",&sel1);
  thetree->Branch("met_x", &met_x);
  thetree->Branch("met_y", &met_y);
  thetree->Branch("met", &met);
  thetree->Branch("leaLep_pt",&leaLep_pt);
  thetree->Branch("leaLep_phi",&leaLep_phi);
  thetree->Branch("leaLep_eta",&leaLep_eta);
  thetree->Branch("photon_pt",&photon_pt);
  thetree->Branch("photon_phi",&photon_phi);
  thetree->Branch("photon_eta",&photon_eta);
  thetree->Branch("ht",&ht);
  thetree->Branch("pt_lyMET",&pt_lyMET);
  thetree->Branch("pq",&pq);

  thetree->Branch("nSelectedJets",&nSelectedJets);
    thetree->Branch("nLep",&nLep);
    thetree->Branch("nPhoton",&nPhoton);
    thetree->Branch("photon2_pt",&photon2_pt);
    thetree->Branch("photon3_pt",&photon3_pt);
    thetree->Branch("photon2_phi",&photon2_phi);
    thetree->Branch("photon3_phi",&photon3_phi);
    thetree->Branch("photon2_eta",&photon2_eta);
    thetree->Branch("photon3_eta",&photon3_eta);
    thetree->Branch("lepton2_pt",&lepton2_pt);
    thetree->Branch("lepton2_phi",&lepton2_phi);
    thetree->Branch("lepton2_eta",&lepton2_eta);
    thetree->Branch("lepton3_pt",&lepton3_pt);
    thetree->Branch("lepton3_phi",&lepton3_phi);
    thetree->Branch("lepton3_eta",&lepton3_eta);
    thetree->Branch("lepPho_dR",&lepPho_dR);
    thetree->Branch("leaJet_pt",&leaJet_pt);
    thetree->Branch("leaJet_phi",&leaJet_phi);
    thetree->Branch("leaJet_eta",&leaJet_eta);
    thetree->Branch("subLeaJet_pt",&subLeaJet_pt);
    thetree->Branch("subLeaJet_phi",&subLeaJet_phi);
    thetree->Branch("subLeaJet_eta",&subLeaJet_eta);
    thetree->Branch("passOR",&passOR);
    thetree->Branch("pho_truType",&pho_truType);
    thetree->Branch("pho_truOrigin",&pho_truOrigin);
    thetree->Branch("pho_topoEtCone20",&pho_topoEtCone20);
    thetree->Branch("pho_topoEtCone40",&pho_topoEtCone40);
    thetree->Branch("photonIsoE",&photonIsoE);
    thetree->Branch("dsid",&dsid);
    thetree->Branch("phoPID_Loose",&phoPID_Loose);
    thetree->Branch("phoPID_Tight",&phoPID_Tight);
    thetree->Branch("phoIso_FixedCutTightCaloOnly",&phoIso_FixedCutTightCaloOnly);
    thetree->Branch("phoIso_FixedCutTight",&phoIso_FixedCutTight);
    thetree->Branch("phoIso_Tight",&phoIso_Tight);
    thetree->Branch("phoIso_FixedCutLoose",&phoIso_FixedCutLoose);
    thetree->Branch("phoIso_Loose",&phoIso_Loose);
    thetree->Branch("electronEvent",&electronEvent);
    thetree->Branch("lepPID_Loose",&lepPID_Loose);
    thetree->Branch("lepPID_Medium",&lepPID_Medium);
    thetree->Branch("lepPID_Tight",&lepPID_Tight);
    thetree->Branch("lepIso_FCLoose",&lepIso_FCLoose);
    thetree->Branch("lepIso_FCTight",&lepIso_FCTight);
    thetree->Branch("weight_all",&weight_all);
    thetree->Branch("weight_mc",&weight_mc);
    thetree->Branch("weight_prw",&weight_prw);
    thetree->Branch("weight_gam",&weight_gam);
    thetree->Branch("weight_e",&weight_e);
    thetree->Branch("weight_m",&weight_m);
    thetree->Branch("elec_truthType",&elec_truthType);
    thetree->Branch("elec_truthOrigin",&elec_truthOrigin);
    thetree->Branch("muon_truthType",&muon_truthType);
    thetree->Branch("muon_truthOrigin",&muon_truthOrigin);
    thetree->Branch("pass_trigger",&pass_trigger);
    thetree->Branch("pass_dq",&pass_dq);
    thetree->Branch("pass_jetclean",&pass_jetclean);
    thetree->Branch("passKinematics",&passKinematics);
    thetree->Branch("weight_vertex",&weight_vertex);
//cxxInsertTree
}

void TreeSkeleton :: initialize_leaves(){

  // this should be called before filling any value to variables
  // such as the begining of execute() of the event selection

  // event basic info
  runnumber = 0; ///< Run number
  eventnumber = 0; ///< Event number
  dsid = 0;

  // selection
  sel1=false;
 
  // variables:
  met_x = 0;
  met_y = 0;
  met = 0;
  leaLep_pt=0;
  leaLep_phi=0;
  leaLep_eta=0;
  photon_pt=0;
  photon_phi=0;
  photon_eta=0;
  ht=0;
  pt_lyMET=0;
  pq=0;
  nSelectedJets=0;
  nLep=0;
  nPhoton=0;
  photon2_pt=0;
  photon3_pt=0;
  photon2_phi=0;
  photon3_phi=0;
  photon2_eta=0;
  photon3_eta=0;
  lepton2_pt=0;
  lepton2_phi=0;
  lepton2_eta=0;
  lepton3_pt=0;
  lepton3_phi=0;
  lepton3_eta=0;
  lepPho_dR=0;
  leaJet_pt=0;
  leaJet_phi=0;
  leaJet_eta=0;
  subLeaJet_pt=0;
  subLeaJet_phi=0;
  subLeaJet_eta=0;
  passOR=0;
  pho_truType=-99;
  pho_truOrigin=-99;
  pho_topoEtCone20=-100000000000;
  pho_topoEtCone40=-100000000000;
  photonIsoE=0;
  dsid=0;
  phoPID_Loose=0;
  phoPID_Tight=0;
  phoIso_FixedCutTightCaloOnly=0;
  phoIso_FixedCutTight=0;
  phoIso_Tight=0;
  phoIso_FixedCutLoose=0;
  phoIso_Loose=0;
  electronEvent=0;
  lepPID_Loose=0;
  lepPID_Medium=0;
  lepPID_Tight=0;
  lepIso_FCLoose=0;
  lepIso_FCTight=0;
  weight_all=1.0;
  weight_mc=1.0;
  weight_prw=1.0;
  weight_gam=1.0;
  weight_e=1.0;
  weight_m=1.0;
  elec_truthType=-99;
  elec_truthOrigin=-99;
  muon_truthType=-99;
  muon_truthOrigin=-99;
  pass_trigger=0;
  pass_dq=0;
  pass_jetclean=0;
  passKinematics=0;
  weight_vertex=1.0;
//cxxInsertVariable

}
